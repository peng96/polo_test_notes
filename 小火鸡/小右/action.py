#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time        : 2021/9/30 下午2:59
# @Author      : Mini-Right
# @Email       : www@anyu.wang
# @File        : browser.py
# @Software    : PyCharm
# @Description : 浏览器操作

"""
    浏览器操作
        最大化浏览器
        最小化浏览器
        设置浏览器大小
        获取浏览器大小
        获取浏览器名称
        浏览器前进
        浏览器后退
        浏览器刷新
        获取当前标签页title
        获取当前标签页的url
        获取当前标签页完整的HTML代码
        获取当前标签页句柄
        获取所有标签页句柄
        切换标签页
        新建标签页
        关闭当前标签页
        关闭所有标签页
        同步执行js
        异步执行js

    元素操作
        元素点击
        输入
        清除内容
        获取元素尺寸
        获取元素坐标
        获取元素标签文本
        获取元素属性
        元素是否可见
        元素是否可点击
        元素是否被选中
        提交表单
"""

from typing import List

from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver import Chrome


class Action(object):
    def __init__(self, driver: Chrome):
        self.driver = driver

    """
    浏览器操作
    """
    def browser_maximize_window(self):
        """
        最大化浏览器
        """
        self.driver.maximize_window()
        return self.driver

    def browser_minimize_window(self):
        """
        最小化浏览器
        """
        self.driver.minimize_window()
        return self.driver

    def browser_set_window_size(self, width: int, height: int):
        """
        设置浏览器大小
        :param width:   宽
        :param height:  高
        """
        self.driver.set_window_size(width=width, height=height)
        return self.driver

    def browser_get_window_size(self):
        """
        获取浏览器大小
        """
        return self.driver.get_window_size()

    def browser_get_driver_name(self):
        """
        获取浏览器名称
        """
        return self.driver.name

    def browser_forward(self):
        """
        浏览器前进
        """
        self.driver.forward()
        return self.driver

    def browser_back(self):
        """
        浏览器后退
        """
        self.driver.back()
        return self.driver

    def browser_refresh(self):
        """
        浏览器刷新
        """
        self.driver.refresh()
        return self.driver

    def browser_get_current_sheet_title(self) -> str:
        """
        获取当前标签页title
        """
        return self.driver.title

    def browser_get_current_sheet_url(self) -> str:
        """
        获取当前标签页的url
        """
        return self.driver.current_url

    def browser_get_current_sheet_page_source(self):
        """
        获取当前标签页完整的HTML代码
        """
        return self.driver.page_source

    def browser_get_current_sheet_handle(self) -> str:
        """
        获取当前标签页句柄
        """
        return self.driver.current_window_handle

    def browser_get_all_sheet_handle(self) -> List[str]:
        """
        获取所有标签页句柄
        """
        return self.driver.window_handles

    def browser_switch_to_sheet(self, handle):
        """
        切换标签页
        :param handle: 标签页句柄
        """
        self.driver.switch_to.window(handle)
        return self.driver

    def browser_create_sheet(self, url: str = None):
        """
        新建标签页
        :param url: url地址
        """
        js_script = f"""window.open("{url}")"""
        self.driver.execute_script(js_script)
        current_handle = self.browser_get_all_sheet_handle()[-1]
        self.browser_switch_to_sheet(current_handle)
        return self.driver

    def browser_close_current_sheet(self):
        """
        关闭当前标签页 如果只有一个就关闭浏览器
        """
        self.driver.close()
        last_handle_list = self.browser_get_all_sheet_handle()
        if len(last_handle_list) != 0:
            last_handle = last_handle_list[-1]
            self.browser_switch_to_sheet(last_handle)
        return self.driver

    def browser_close_all_sheet(self):
        """
        关闭所有标签页
        """
        for handle in self.browser_get_all_sheet_handle():
            self.browser_switch_to_sheet(handle)
            self.browser_close_current_sheet()
        return self.driver

    def browser_execute(self, js_script: str):
        """
        同步执行js
        :param js_script: js代码
        """
        self.driver.execute_script(js_script)
        return self.driver

    def browser_execute_async(self, js_script: str):
        """
        异步执行js
        :param js_script: js代码
        """
        self.driver.execute_async_script(js_script)
        return self.driver

    """
    元素操作
    """
    @staticmethod
    def element_click(element: WebElement):
        """
        元素点击
        """
        element.click()
        return element

    @staticmethod
    def element_send_keys(element: WebElement, value: str):
        """
        输入
        """
        element.send_keys(value)
        return element

    @staticmethod
    def element_clear(element: WebElement):
        """
        清除内容
        """
        element.clear()
        return element

    @staticmethod
    def element_get_size(element: WebElement) -> dict:
        """
        获取元素尺寸
        :return: {'height': 23, 'width': 42}
        """
        return element.size

    @staticmethod
    def element_get_location(element: WebElement) -> dict:
        """
        获取元素坐标
        :return: {'x': 457, 'y': 8}
        """
        return element.location

    @staticmethod
    def element_get_text(element: WebElement, to_list: bool = False) -> str:
        """
        获取元素标签文本
        当to_list为True且获取文本只有一个时 不反回列表
        """
        text: str = element.text
        if to_list is True:
            text_list = text.split('\n')
            return text if len(text_list) == 1 else text_list
        return text

    @staticmethod
    def element_get_attribute(element: WebElement, attr_name: str):
        """
        获取元素属性
        :return:
        """
        return element.get_attribute(attr_name)

    @staticmethod
    def element_is_displayed(element: WebElement):
        """
        元素是否可见
        :return:
        """
        return element.is_displayed()

    @staticmethod
    def element_is_enabled(element: WebElement):
        """
        元素是否可点击
        :return:
        """
        return element.is_enabled()

    @staticmethod
    def element_is_selected(element: WebElement):
        """
        元素是否被选中
        :return:
        """
        return element.is_selected()

    @staticmethod
    def element_submit(element: WebElement):
        """
        提交表单
        :return:
        """
        element.submit()
        return element


